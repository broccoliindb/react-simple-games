import React, { useState, useEffect, useRef, memo } from 'react'
import styled from 'styled-components'

const RSPImg = styled.div`
  width: 150px;
  height: 200px;
  padding: 18px;
  background-image: url('../assets/rsp.jpg');
  border: 1px solid black;
  &.scissors {
    background-position: 0px;
  }
  &.rock {
    background-position: -150px;
  }
  &.paper {
    background-position: -300px;
  }
`

const Buttons = styled.div`
  margin: 10px;
  button {
    margin: 5px;
  }
`

const RSPTypes = {
  rock: { name: '주먹', score: 1 },
  scissors: { name: '가위', score: 0 },
  paper: { name: '보', score: -1 }
}

const RSP = () => {
  const [state, setState] = useState('rock')
  const [result, setResult] = useState('')
  const [score, setScore] = useState(0)
  const interval = useRef(null)
  const t = useRef(null)

  const debounce = (func) => {
    clearTimeout(t.current)
    t.current = setTimeout(() => {
      func()
    }, 3000)
  }
  const setChangeRSP = () => {
    if (state === 'rock') {
      setState('scissors')
    } else if (state === 'scissors') {
      setState('paper')
    } else {
      setState('rock')
    }
  }
  const onSelectRSP = (type) => (e) => {
    clearInterval(interval.current)
    const diff = RSPTypes[state].score - RSPTypes[type].score
    if (diff === 0) {
      setResult('draw')
      setScore((prev) => prev)
    } else if (diff === -1 || diff === 2) {
      setResult('win')
      setScore((prev) => prev + 1)
    } else {
      setResult('lose')
      setScore((prev) => prev - 1)
    }
    debounce(() => {
      interval.current = setInterval(setChangeRSP, 500)
    })
    // setTimeout(() => {
    //   interval.current = setInterval(setChangeRSP, 1000)
    // }, 1000)
  }

  useEffect(() => {
    interval.current = setInterval(setChangeRSP, 500)
    return () => {
      clearInterval(interval.current)
    }
  }, [state])

  return (
    <>
      <RSPImg className={state}></RSPImg>
      <Buttons>
        <button onClick={onSelectRSP('rock')}>주먹</button>
        <button onClick={onSelectRSP('scissors')}>가위</button>
        <button onClick={onSelectRSP('paper')}>보</button>
      </Buttons>
      <div>{result}</div>
      <div>{score}</div>
    </>
  )
}

export default memo(RSP)
