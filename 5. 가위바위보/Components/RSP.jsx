import React, { Component } from 'react'
import styled from 'styled-components'

const RSPImg = styled.div`
  width: 150px;
  height: 200px;
  padding: 18px;
  background-image: url('../assets/rsp.jpg');
  border: 1px solid black;
  &.scissors {
    background-position: 0px;
  }
  &.rock {
    background-position: -150px;
  }
  &.paper {
    background-position: -300px;
  }
`

const Buttons = styled.div`
  margin: 10px;
  button {
    margin: 5px;
  }
`

const RSPTypes = {
  rock: { name: '주먹', score: 1 },
  scissors: { name: '가위', score: 0 },
  paper: { name: '보', score: -1 }
}

class RSP extends Component {
  state = {
    state: 'rock',
    result: '',
    score: 0
  }

  interval

  setChangeRSP = () => {
    const { state } = this.state
    console.log('1', state, this)
    if (state === 'rock') {
      this.setState({
        state: 'scissors'
      })
    } else if (state === 'scissors') {
      this.setState({
        state: 'paper'
      })
    } else {
      this.setState({
        state: 'rock'
      })
    }
  }
  componentDidMount() {
    this.interval = setInterval(this.setChangeRSP, 100)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  onSelectRSP = (type) => (e) => {
    const { state } = this.state
    clearInterval(this.interval)
    console.log(state, type, RSPTypes[type])
    const diff = RSPTypes[state].score - RSPTypes[type].score
    if (diff === 0) {
      this.setState({ result: 'draw.' })
    } else if (diff === -1 || diff === 2) {
      this.setState((prev) => {
        return { result: 'win.', score: prev.score + 1 }
      })
    } else {
      this.setState((prev) => {
        return { result: 'lose.', score: prev.score - 1 }
      })
    }
    setTimeout(() => {
      this.interval = setInterval(this.setChangeRSP, 100)
    }, 1000)
  }
  render() {
    const { state, result, score } = this.state
    return (
      <>
        <RSPImg className={state}></RSPImg>
        <Buttons>
          <button onClick={this.onSelectRSP('rock')}>주먹</button>
          <button onClick={this.onSelectRSP('scissors')}>가위</button>
          <button onClick={this.onSelectRSP('paper')}>보</button>
        </Buttons>
        <div>{result}</div>
        <div>{score}</div>
      </>
    )
  }
}

export default RSP
