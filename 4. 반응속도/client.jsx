import React from 'react'
import ReactDom from 'react-dom'
// import CheckReactionTime from './Components/CheckReactionTime'
import CheckReactionTime from './Components/FunctionalCheckReactionTime'

ReactDom.render(<CheckReactionTime />, document.querySelector('#root'))
