import React, { memo } from 'react'

const Average = memo(({ results }) => {
  return (
    <div>평균시간: {results.reduce((a, c) => a + c) / results.length}(ms)</div>
  )
})

export default Average
