import React, { PureComponent } from 'react'

class Average extends PureComponent {
  render() {
    const { results } = this.props
    return (
      <div>
        평균시간: {results.reduce((a, c) => a + c) / results.length}(ms)
      </div>
    )
  }
}

export default Average
