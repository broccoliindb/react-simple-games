import React, { memo, useState, useRef } from 'react'
import styled from 'styled-components'
import Average from '../Components/Average'

const Container = styled.div`
  width: 20rem;
  height: 20rem;
  margin: 1rem;
  &.waiting {
    background-color: red;
  }
  &.ready {
    background-color: yellow;
  }
  &.now {
    background-color: green;
    color: white;
  }
  &.finished {
    background-color: blue;
    color: white;
  }
`

const CheckReactionTime = () => {
  const [state, setState] = useState('waiting')
  const [message, setMessage] = useState('클릭하시면 게임을 진행합니다.')
  const [results, setResults] = useState([])
  const start = useRef(null)
  const end = useRef(null)
  const timeout = useRef(null)

  const resetHandler = () => {
    setState('waiting')
    setMessage('클릭하시면 게임을 진행합니다.')
    setResults([])
  }
  const checkReactionTimeHandler = () => {
    if (state === 'waiting') {
      setState('ready')
      setMessage('초록색일때 클릭하세요')

      timeout.current = setTimeout(() => {
        start.current = new Date()
        setState('now')
        setMessage('빨리 클릭하세요!!!')
      }, Math.floor(Math.random() * 1000 + 2000))
    } else if (state === 'now') {
      end.current = new Date()
      setState('finished')
      setMessage('다시하기!')
      setResults((prevResult) => {
        return [...prevResult, end.current - start.current]
      })
    } else if (state === 'finished') {
      setState('waiting')
      setMessage('클릭하시면 게임을 진행합니다.')
    } else if (state === 'ready') {
      clearTimeout(timeout.current)
      setMessage('너무 성급하셨습니다!!! 초록색일때 클릭하세요')
      timeout.current = setTimeout(() => {
        start.current = new Date()
        setState('now')
        setMessage('빨리 클릭하세요!!!')
      }, Math.floor(Math.random() * 1000 + 2000))
    }
  }
  return (
    <>
      <h1>반응속도체크 게임</h1>
      <Container onClick={checkReactionTimeHandler} className={state}>
        {message}
      </Container>
      {results.length !== 0 && <Average results={results}></Average>}
      <button onClick={resetHandler}>평균데이터 모두 리셋</button>
    </>
  )
}

export default CheckReactionTime
