import React, { PureComponent } from 'react'
import styled from 'styled-components'
import Average from '../Components/Average'

const Container = styled.div`
  width: 20rem;
  height: 20rem;
  margin: 1rem;
  &.waiting {
    background-color: red;
  }
  &.ready {
    background-color: yellow;
  }
  &.now {
    background-color: green;
    color: white;
  }
  &.finished {
    background-color: blue;
    color: white;
  }
`

class CheckReactionTime extends PureComponent {
  state = {
    state: 'waiting',
    message: '클릭하시면 게임을 진행합니다.',
    results: []
  }
  start
  end
  timeout
  resetHandler = () => {
    const { state, message, results } = this.state
    this.setState({
      state: 'waiting',
      message: '클릭하시면 게임을 진행합니다.',
      results: []
    })
  }
  checkReactionTimeHandler = () => {
    const { state, message, results } = this.state
    if (state === 'waiting') {
      this.setState({
        state: 'ready',
        message: '초록색일때 클릭하세요'
      })
      this.timeout = setTimeout(() => {
        this.start = new Date()
        this.setState({
          state: 'now',
          message: '빨리 클릭하세요!!!'
        })
      }, Math.floor(Math.random() * 1000 + 2000))
    } else if (state === 'now') {
      this.end = new Date()
      this.setState({
        state: 'finished',
        message: '다시하기!',
        results: [...results, this.end - this.start]
      })
    } else if (state === 'finished') {
      this.setState({
        state: 'waiting',
        message: '클릭하시면 게임을 진행합니다.'
      })
    } else if (state === 'ready') {
      clearTimeout(this.timeout)
      this.setState({
        message: '너무 성급하셨습니다!!! 초록색일때 클릭하세요'
      })
      this.timeout = setTimeout(() => {
        this.start = new Date()
        this.setState({
          state: 'now',
          message: '빨리 클릭하세요!!!'
        })
      }, Math.floor(Math.random() * 1000 + 2000))
    }
  }

  render() {
    const { state, message, results } = this.state
    return (
      <>
        <h1>반응속도체크 게임</h1>
        <Container onClick={this.checkReactionTimeHandler} className={state}>
          {message}
        </Container>
        {results.length !== 0 && <Average results={results}></Average>}
        <button onClick={this.resetHandler}>평균데이터 모두 리셋</button>
      </>
    )
  }
}

export default CheckReactionTime
