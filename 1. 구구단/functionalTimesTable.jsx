const React = require('react')

const TimesTable = () => {
  const [first, setFirst] = React.useState(Math.ceil(Math.random() * 9))
  const [second, setSecond] = React.useState(Math.ceil(Math.random() * 9))
  const [result, setResult] = React.useState('')
  const [value, setValue] = React.useState('')
  const onInputRef = React.useRef(null)
  const handleChange = (evt) => {
    setValue(evt.target.value)
  }
  const init = () => {
    setFirst(Math.ceil(Math.random() * 9))
    setSecond(Math.ceil(Math.random() * 9))
    setValue('')
    setResult('')
  }

  React.useEffect(() => {
    if (result === '정답') {
      setTimeout(init, 500)
    }
  }, [result])

  React.useEffect(() => {
    onInputRef.current.focus()
  }, [])

  const handleSubmit = (evt) => {
    evt.preventDefault()
    if (value) {
      if (parseInt(value, 10) === first * second) {
        setResult('정답')
      } else {
        setResult('틀렸습니다.')
        setValue('')
      }
    }
  }
  return (
    <>
      <div>
        {first} 곱하기 {second} 은(는)?
      </div>
      <form onSubmit={handleSubmit}>
        <input
          ref={onInputRef}
          type="number"
          value={value}
          onChange={handleChange}
        />
        <button type="submit">입력</button>
      </form>
      <div>{result}</div>
    </>
  )
}

module.exports = TimesTable
