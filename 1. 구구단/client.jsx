const React = require('react')
const ReactDom = require('react-dom')
// const TimesTable = require('./TimesTable')
const TimesTable = require('./functionalTimesTable')

ReactDom.render(<TimesTable />, document.querySelector('#root'))
