const path = require('path')
const { webpack } = require('webpack')

module.exports = {
  name: 'times table webpack config',
  devtool: 'eval', //hidden-source-map
  mode: 'development',
  resolve: {
    extensions: ['.jsx', '.js']
  },
  entry: {
    app: ['./client']
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: ['@babel/plugin-proposal-class-properties']
        }
      }
    ]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({debug: true})
  ]
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'app.js'
  }
}
