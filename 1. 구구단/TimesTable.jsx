const React = require('react')

class TimesTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      first: Math.ceil(Math.random() * 9),
      second: Math.ceil(Math.random() * 9),
      value: '',
      result: ''
    }
  }
  init = () => {
    this.setState({
      first: Math.ceil(Math.random() * 9),
      second: Math.ceil(Math.random() * 9),
      value: '',
      result: ''
    })
  }

  onChange = (evt) => {
    this.setState({ value: evt.target.value })
  }

  input
  onInputRef = (input) => {
    this.input = input
  }

  submitValues = (evt) => {
    evt.preventDefault()
    this.input.focus()
    if (this.state.value) {
      const isCorrect =
        parseInt(this.state.value, 10) === this.state.first * this.state.second
      if (isCorrect) {
        this.setState({ result: `정답[${this.state.value}] 맞았습니다.` })
        setTimeout(this.init, 500)
      } else {
        this.setState({
          result: '틀렸습니다.',
          value: ''
        })
      }
    }
  }

  componentDidMount() {
    this.input.focus()
  }

  render() {
    return (
      <>
        <div>
          {this.state.first} 곱하기 {this.state.second}
        </div>
        <form onSubmit={this.submitValues}>
          <input
            ref={this.onInputRef}
            type="number"
            value={this.state.value}
            onChange={this.onChange}
          />
          <button>입력</button>
        </form>
        <div>{this.state.result}</div>
      </>
    )
  }
}

module.exports = TimesTable
