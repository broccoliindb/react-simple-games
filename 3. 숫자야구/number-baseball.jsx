const React = require('react')
const { Component } = React

const getRandom = () => {
  const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  const selected = []
  for (let i = 0; i < 4; i++) {
    selected.push(numbers.splice(Math.floor(Math.random() * 9 - i), 1)[0])
  }
  console.log(selected)
  return selected
}

class NumberBaseball extends Component {
  state = {
    value: '',
    results: '',
    numbers: getRandom(),
    tries: []
  }
  onChangeHandler = (evt) => {
    this.setState({ value: evt.currentTarget.value })
    if (evt.currentTarget.value.length > 4) {
      this.setState({
        value: '',
        results: 'Too many numbers. Type 4 Numbers'
      })
    }
  }
  onSubmitHandler = (evt) => {
    evt.preventDefault()
    if (this.state.value === this.state.numbers.join('')) {
      this.setState((prevTries) => {
        console.log('prev1', prevTries)
        return {
          results: '홈런',
          tries: [...prevTries.tries, { try: this.state.value, result: '홈런' }]
        }
      })
      alert('홈런입니다. 게임을 초기화 합니다.')
      this.setState({
        value: '',
        results: '',
        tries: [],
        numbers: getRandom()
      })
    } else if (this.state.tries.length > 9) {
      alert('10회 이상 틀렸습니다. 게임을 초기화 합니다.')
      this.setState({
        results: '',
        tries: [],
        numbers: getRandom(),
        value: ''
      })
    } else {
      let strikeCount = 0
      let ballCount = 0
      const triValues = this.state.value.split('').map((n) => +n)
      for (let i = 0; i < 4; i++) {
        if (triValues[i] === this.state.numbers[i]) {
          strikeCount++
        } else if (this.state.numbers.includes(triValues[i])) {
          ballCount++
        }
      }
      this.setState((prevTries) => {
        console.log('prev2', prevTries)
        return {
          results: `시도: ${triValues.join(
            ''
          )}, 스트라이크: ${strikeCount}, 볼: ${ballCount}`,
          tries: [
            ...prevTries.tries,
            {
              try: this.state.value,
              result: `시도: ${triValues.join(
                ''
              )}, 스트라이크: ${strikeCount}, 볼: ${ballCount}`
            }
          ],
          value: ''
        }
      })
    }
  }

  input
  onInputRef = (c) => {
    this.input = c
  }

  componentDidMount() {
    this.input.focus()
  }

  render() {
    const { value, results, tries } = this.state
    return (
      <>
        <h1>숫자야구</h1>
        <p>
          1~9까지의 숫자중 4개를 중복되지 않게 고르라. 선택된 랜덤한 수를
          일치시키면 스트라이크 아니면 볼이며 10회 이하의 횟수로 성공해야 한다.
        </p>
        <form onSubmit={this.onSubmitHandler}>
          <input
            ref={this.onInputRef}
            type="number"
            value={value}
            onChange={this.onChangeHandler}
          />
          <button type="submit">입력</button>
        </form>
        <div>{results}</div>
        <ul>
          {tries.map((i, index) => (
            <li key={i.result + index}>
              {index + 1}회 : {i.result}
            </li>
          ))}
        </ul>
      </>
    )
  }
}

module.exports = NumberBaseball
