import React, { memo } from 'react'

const Try = memo(({ t, index }) => {
  return (
    <>
      <li key={t.result + index}>
        {index + 1}회 : {t.result}
      </li>
    </>
  )
})

export default Try
