import React, { useState, useEffect, useRef, memo } from 'react'
import Try from './Try'

const getRandom = () => {
  const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  const selected = []
  for (let i = 0; i < 4; i++) {
    selected.push(numbers.splice(Math.floor(Math.random() * 9 - i), 1)[0])
  }
  return selected
}

const NumberBaseball = memo(() => {
  const [value, setValue] = useState('')
  const [results, setResults] = useState('')
  const [numbers, setNumbers] = useState(getRandom())
  const [tries, setTries] = useState([])
  const onInputRef = useRef(null)
  console.log(numbers)

  const onChangeHandler = (evt) => {
    setValue(evt.currentTarget.value)
    if (evt.currentTarget.value.length > 4) {
      setValue('')
      setResults('Too many numbers. Type 4 Numbers')
    }
  }

  const onSubmitHandler = (evt) => {
    evt.preventDefault()
    setValue('')
    if (value === numbers.join('')) {
      setResults('홈런')
      setTries((prevTries) => {
        return [...prevTries, { try: value, result: '홈런' }]
      })
      alert('홈런입니다. 게임을 초기화 합니다.')
      setResults(() => '')
      setTries([])
      setNumbers(getRandom())
    } else if (tries.length > 9) {
      setResults('틀렸습니다')
      alert('10회 이상 틀렸습니다. 게임을 초기화 합니다.')
      setResults(() => '')
      setTries([])
      setNumbers(getRandom())
    } else {
      let strikeCount = 0
      let ballCount = 0
      const triValues = value.split('').map((n) => +n)
      for (let i = 0; i < 4; i++) {
        if (triValues[i] === numbers[i]) {
          strikeCount++
        } else if (numbers.includes(triValues[i])) {
          ballCount++
        }
      }
      setResults('틀렸습니다')
      setTries((prevTries) => {
        return [
          ...prevTries,
          {
            try: value,
            result: `시도: ${triValues.join(
              ''
            )}, 스트라이크: ${strikeCount}, 볼: ${ballCount}`
          }
        ]
      })
    }
  }

  useEffect(() => {
    onInputRef.current.focus()
  }, [])
  return (
    <>
      <h1>숫자야구</h1>
      <p>
        1~9까지의 숫자중 4개를 중복되지 않게 고르라. 선택된 랜덤한 수를
        일치시키면 스트라이크 아니면 볼이며 10회 이하의 횟수로 성공해야 한다.
      </p>
      <form onSubmit={onSubmitHandler}>
        <input
          ref={onInputRef}
          type="number"
          value={value}
          onChange={onChangeHandler}
        />
        <button type="submit">입력</button>
      </form>
      <div>{results}</div>
      <ul>
        {tries.map((t, index) => (
          <Try key={t.result + index} t={t} index={index}></Try>
        ))}
      </ul>
    </>
  )
})

export default NumberBaseball
