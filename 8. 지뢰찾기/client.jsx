import React from 'react'
import ReactDom from 'react-dom'
import SearchMine from './components/SearchMine'

ReactDom.render(<SearchMine />, document.querySelector('#root'))
