import React, { memo } from 'react'
import Td from './Td'

const Tr = ({ row, rowIndex }) => {
  return (
    <tr>
      {row.map((cell, index) => (
        <Td cell={cell} rowIndex={rowIndex} cellIndex={index} key={index} />
      ))}
    </tr>
  )
}
export default memo(Tr)
