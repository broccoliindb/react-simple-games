import React, { useCallback, useContext, memo } from 'react'
import styled from 'styled-components'
import {
  CLICK_CELL,
  RIGHT_CLICK_CELL,
  STATUS,
  TableContext
} from './SearchMine'

const STd = styled.td`
  border: 1px solid black;
  width: 3rem;
  height: 3rem;
  text-align: center;
  background-color: ${(props) =>
    props.type === STATUS.NORMAL || props.type === STATUS.MINE
      ? 'grey'
      : props.type === STATUS.QUESTION || props.type === STATUS.QUESTION_MINE
      ? 'yellow'
      : props.type === STATUS.FLAG || props.type === STATUS.FLAG_MINE
      ? 'purple'
      : props.type === STATUS.BOMB
      ? 'red'
      : 'white'};
`

const Td = ({ cell, rowIndex, cellIndex }) => {
  const { dispatch, tableData } = useContext(TableContext)

  const TdText = useCallback(
    (cell, rowIndex, cellIndex) => {
      switch (cell) {
        case STATUS.NORMAL:
          return ''
        case STATUS.MINE:
          return 'X'
        case STATUS.FLAG_MINE:
        case STATUS.FLAG:
          return '!'
        case STATUS.QUESTION:
        case STATUS.QUESTION_MINE:
          return '?'
        case STATUS.BOMB:
          return '퐝'
        default:
          return tableData[rowIndex][cellIndex] === 0
            ? ''
            : tableData[rowIndex][cellIndex]
      }
    },
    [tableData, rowIndex, cellIndex, cell]
  )

  const onCellOpen = useCallback(
    (e) => {
      dispatch({
        type: CLICK_CELL,
        row: rowIndex,
        cell: cellIndex,
        value: cell
      })
    },
    [tableData]
  )
  const onCellRightClick = useCallback(
    (e) => {
      e.preventDefault()
      dispatch({
        type: RIGHT_CLICK_CELL,
        row: rowIndex,
        cell: cellIndex,
        value: cell
      })
    },
    [tableData]
  )
  return (
    <STd type={cell} onClick={onCellOpen} onContextMenu={onCellRightClick}>
      {TdText(cell, rowIndex, cellIndex)}
    </STd>
  )
}
export default memo(Td)
