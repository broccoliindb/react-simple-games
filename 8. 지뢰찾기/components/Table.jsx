import React, { memo } from 'react'
import Tr from './Tr'
import styled from 'styled-components'

const STable = styled.table`
  border-collapse: collapse;
`

const Table = ({ tableData }) => {
  return (
    <STable>
      {tableData.map((row, index) => (
        <Tr row={row} rowIndex={index} key={index} />
      ))}
    </STable>
  )
}
export default memo(Table)
