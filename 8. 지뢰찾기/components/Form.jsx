import React, { useState, useCallback, useContext, memo } from 'react'
import { SET_MINE_TABLE, TableContext } from './SearchMine'

const Form = () => {
  const [row, setRow] = useState(5)
  const [cell, setCell] = useState(5)
  const [mine, setMine] = useState(5)
  const { dispatch } = useContext(TableContext)
  const onChangeRow = useCallback(
    (e) => {
      setRow(e.currentTarget.value)
    },
    [row]
  )
  const onChangeCell = useCallback(
    (e) => {
      setCell(e.currentTarget.value)
    },
    [cell]
  )
  const onChangeMine = useCallback(
    (e) => {
      setMine(e.currentTarget.value)
    },
    [mine]
  )
  const onSubmitHandler = useCallback(
    (e) => {
      e.preventDefault()
      dispatch({ type: SET_MINE_TABLE, row: +row, cell: +cell, mine: +mine })
    },
    [row, cell, mine]
  )

  return (
    <form onSubmit={onSubmitHandler}>
      <input
        type="number"
        placeholder="세로"
        value={row}
        onChange={onChangeRow}
        minLength={1}
      />
      <input
        type="number"
        placeholder="가로"
        value={cell}
        onChange={onChangeCell}
        minLength={1}
      />
      <input
        type="number"
        placeholder="지뢰"
        value={mine}
        onChange={onChangeMine}
      />
      <button type="submit">입력</button>
    </form>
  )
}

export default memo(Form)
