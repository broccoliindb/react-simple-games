import React, {
  useReducer,
  useEffect,
  createContext,
  useMemo,
  memo
} from 'react'
import Table from './Table'
import Form from './Form'

export const TableContext = createContext({
  tableData: [],
  dispatch: () => {}
})

// ACTION TYPE
export const SET_MINE_TABLE = 'SET_MINE_TABLE'
export const CLICK_CELL = 'CLICK_CELL'
export const RIGHT_CLICK_CELL = 'RIGHT_CLICK_CELL'
export const INCREMENT_TIMER = 'INCREMENT_TIMER'

// MINE STATUS
export const STATUS = {
  MINE: -7,
  NORMAL: -1,
  FLAG: -2,
  FLAG_MINE: -3,
  QUESTION: -4,
  QUESTION_MINE: -5,
  BOMB: -6,
  OPEN: 0
}

const initState = {
  tableData: [],
  finished: null,
  openCellCount: 0,
  data: {
    row: -1,
    cell: -1,
    mine: -1
  },
  result: '',
  timer: 0
}

const setTableData = (rowCount, cellCount, mineCount) => {
  const dataTable = Array(rowCount)
    .fill('')
    .map((row) =>
      Array(cellCount)
        .fill(STATUS.NORMAL)
        .map((v) => v)
    )
  const shuffle = []
  const candidate = Array(rowCount * cellCount)
    .fill('')
    .map((_, index) => index)
  for (let i = 0; i < mineCount; i++) {
    shuffle.push(
      candidate.splice(
        Math.floor(Math.random() * (rowCount * cellCount) - i),
        1
      )[0]
    )
  }
  shuffle.forEach((value) => {
    const ver = Math.floor(value / cellCount)
    const hor = value % cellCount
    dataTable[ver][hor] = STATUS.MINE
  })
  return dataTable
}

const setCellData = (table, row, cell, value) => {
  switch (value) {
    case STATUS.NORMAL:
      table[row][cell] = STATUS.OPEN
      break
    case STATUS.FLAG:
    case STATUS.FLAG_MINE:
    case STATUS.QUESTION:
    case STATUS.QUESTION_MINE:
      break
    case STATUS.MINE:
      table[row][cell] = STATUS.BOMB
      break
  }
  return table
}

const setFlag = (table, row, cell, value) => {
  switch (value) {
    case STATUS.NORMAL:
      table[row][cell] = STATUS.FLAG
      break
    case STATUS.MINE:
      table[row][cell] = STATUS.FLAG_MINE
      break
    case STATUS.FLAG:
      table[row][cell] = STATUS.QUESTION
      break
    case STATUS.FLAG_MINE:
      table[row][cell] = STATUS.QUESTION_MINE
      break
    case STATUS.QUESTION:
      table[row][cell] = STATUS.NORMAL
      break
    case STATUS.QUESTION_MINE:
      table[row][cell] = STATUS.MINE
      break
  }
  return table
}

const getMineCount = (row, cell, table) => {
  const y = [-1, -1, 0, 1, 1, 1, 0, -1]
  const x = [0, 1, 1, 1, 0, -1, -1, -1]
  let count = 0
  for (let i = 0; i < 8; i++) {
    if (
      row + y[i] >= 0 &&
      row + y[i] < table.length &&
      cell + x[i] >= 0 &&
      cell + x[i] < table[row].length
    ) {
      if (table[row + y[i]][cell + x[i]] === STATUS.MINE) {
        count++
      }
    }
  }
  return count
}

const reducer = (state, action) => {
  const { tableData, finished, openCellCount, data } = state
  switch (action.type) {
    case SET_MINE_TABLE:
      return {
        ...state,
        finished: false,
        tableData: setTableData(action.row, action.cell, action.mine),
        data: {
          row: action.row,
          cell: action.cell,
          mine: action.mine
        },
        openCellCount: 0,
        result: '',
        timer: 0
      }
    case INCREMENT_TIMER: {
      return {
        ...state,
        timer: state.timer + 1
      }
    }
    case CLICK_CELL: {
      if (finished) {
        return {
          ...state
        }
      }
      const newTableData = [...tableData]
      newTableData.forEach((row, i) => {
        newTableData[i] = [...tableData[i]]
      })
      const checked = []
      let count = 0
      let end = false
      if (action.value === STATUS.NORMAL) {
        count++
      }
      if (action.value === STATUS.MINE) {
        end = true
      }
      const checkAround = (row, cell, table) => {
        if (
          [
            STATUS.OPEN,
            STATUS.QUESTION,
            STATUS.QUESTION_MINE,
            STATUS.FLAG,
            STATUS.FLAG_MINE,
            STATUS.MINE,
            STATUS.BOMB
          ].includes(action.value)
        ) {
          console.log('제외된 좌표', row, cell)
          return
        }
        if (checked.includes(`${row},${cell}`)) {
          return
        } else {
          checked.push(`${row},${cell}`)
        }
        table[row][cell] = getMineCount(row, cell, table)
        if (table[row][cell] === 0) {
          const near = []
          const y = [-1, -1, 0, 1, 1, 1, 0, -1]
          const x = [0, 1, 1, 1, 0, -1, -1, -1]
          for (let i = 0; i < 8; i++) {
            if (
              row + y[i] >= 0 &&
              row + y[i] < table.length &&
              cell + x[i] >= 0 &&
              cell + x[i] < table[row].length
            ) {
              near.push([row + y[i], cell + x[i]])
            }
          }
          near.forEach((n) => {
            if (
              ![STATUS.OPEN, STATUS.QUESTION_MINE, STATUS.FLAG_MINE].includes(
                table[n[0]][n[1]]
              ) &&
              table[n[0]][n[1]] < 0
            ) {
              console.log('좌표', n[0], n[1])
              count++
              checkAround(n[0], n[1], table)
            }
          })
        } else {
        }
      }
      checkAround(
        action.row,
        action.cell,
        setCellData(newTableData, action.row, action.cell, action.value)
      )
      console.log(
        'opencellcount',
        openCellCount + count,
        data.row,
        data.cell,
        data.mine
      )
      let result = ''
      if (openCellCount + count === data.row * data.cell - data.mine) {
        console.log('승리')
        result = '승리하였습니다'
        end = true
      } else if (action.value === STATUS.MINE) {
        end = true
        result = '패배하였습니다'
      } else {
        console.log('아무것도')
        result = ''
      }
      return {
        ...state,
        finished: end,
        tableData: newTableData,
        openCellCount: openCellCount + count,
        result
      }
    }
    case RIGHT_CLICK_CELL: {
      if (finished) {
        return {
          ...state
        }
      }
      const newTableData = [...tableData]
      newTableData[action.row] = [...tableData[action.row]]
      return {
        ...state,
        tableData: setFlag(newTableData, action.row, action.cell, action.value)
      }
    }
    default:
      return state
  }
}

const SearchMine = () => {
  const [state, dispatch] = useReducer(reducer, initState)
  const { tableData, finished, result, timer } = state
  const value = useMemo(
    () => ({
      tableData,
      dispatch
    }),
    [tableData, finished]
  )

  useEffect(() => {
    let timer
    if (finished === false) {
      timer = setInterval(() => {
        dispatch({ type: INCREMENT_TIMER })
      }, 1000)
    }
    return () => {
      clearInterval(timer)
    }
  }, [finished])

  return (
    <TableContext.Provider value={value}>
      <Form />
      <div>{timer}</div>
      <Table tableData={tableData}></Table>
      <div>{result}</div>
    </TableContext.Provider>
  )
}
export default memo(SearchMine)
