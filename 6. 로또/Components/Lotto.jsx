import React, { Component } from 'react'
import styled from 'styled-components'
import Ball from './Ball'

const Container = styled.div`
  display: flex;
  justify-content: flex-start;
`

const PickAgain = styled.button`
  margin-top: 1rem;
`

const getRandomNumbers = () => {
  console.log('getRandomNumbers')
  const numberPools = Array(45)
    .fill()
    .map((v, i) => i + 1)
  const results = []
  for (let i = 0; i < 7; i++) {
    results.push(numberPools.splice(Math.floor(Math.random() * (45 - i)), 1)[0])
  }
  return results
}

class Lotto extends Component {
  state = {
    randomNumbers: getRandomNumbers(),
    winBalls: [],
    bonus: null
  }

  timeouts = []

  setRandomBalls = () => {
    console.log('setRandomBalls')
    const { randomNumbers, winBalls, bonus } = this.state
    for (let i = 0; i < 6; i++) {
      this.timeouts[i] = setTimeout(() => {
        this.setState((prev) => {
          return {
            winBalls: [...prev.winBalls, randomNumbers[i]]
          }
        })
      }, 1000 * i)
    }
    this.timeouts[6] = setTimeout(() => {
      this.setState({
        bonus: randomNumbers[randomNumbers.length - 1]
      })
    }, 6000)
  }
  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate')
    if (this.state.winBalls.length === 0) {
      this.setRandomBalls()
    }
  }

  componentWillUnmount() {
    this.timeouts.forEach((i) => clearTimeout(i))
  }

  componentDidMount() {
    console.log('componentDidMount')
    this.setRandomBalls()
  }

  onPickWinBalls = () => {
    this.setState({
      randomNumbers: getRandomNumbers(),
      winBalls: [],
      bonus: null
    })
    this.timeouts = []
  }

  render() {
    const { randomNumbers, winBalls, bonus } = this.state
    return (
      <>
        <h2>당첨숫자</h2>
        <Container>
          {winBalls.map((i) => (
            <Ball key={i} num={i}></Ball>
          ))}
        </Container>
        {bonus && (
          <>
            <h2>보너스볼</h2>
            <Ball num={bonus}></Ball>
            <PickAgain onClick={this.onPickWinBalls}>다시뽑기</PickAgain>
          </>
        )}
      </>
    )
  }
}

export default Lotto
