import React, { memo } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  border: 1px solid black;
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  margin: 5px;
  background-color: ${(props) =>
    props.num < 10
      ? 'wheat'
      : props.num < 20
      ? 'yellow'
      : props.num < 30
      ? 'pink'
      : props.num < 40
      ? 'orange'
      : 'yellowgreen'};
`

const Ball = ({ num }) => {
  return <Container num={num}>{num}</Container>
}

export default memo(Ball)
