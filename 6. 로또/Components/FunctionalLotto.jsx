import React, { useState, useEffect, useRef, useCallback, useMemo } from 'react'
import styled from 'styled-components'
import Ball from './Ball'

const Container = styled.div`
  display: flex;
  justify-content: flex-start;
`

const PickAgain = styled.button`
  margin-top: 1rem;
`

const getRandomNumbers = () => {
  console.log('getRandomNumbers')
  const numberPools = Array(45)
    .fill()
    .map((v, i) => i + 1)
  const results = []
  for (let i = 0; i < 7; i++) {
    results.push(numberPools.splice(Math.floor(Math.random() * (45 - i)), 1)[0])
  }
  return results
}

const Lotto = () => {
  const pickedNumbers = useMemo(() => getRandomNumbers(), [])
  const [randomNumbers, setRandomNumbers] = useState(pickedNumbers)
  const [winBalls, setWinBalls] = useState([])
  const [bonus, setBonus] = useState(null)
  const timeouts = useRef([])
  const mount = useRef(false)

  const setRandomBalls = useCallback(() => {
    console.log('setRandomBalls')
    console.log(randomNumbers)
    for (let i = 0; i < 6; i++) {
      timeouts.current[i] = setTimeout(() => {
        setWinBalls((prevWinBalls) => [...prevWinBalls, randomNumbers[i]])
      }, 1000 * i)
    }
    timeouts.current[6] = setTimeout(() => {
      setBonus(randomNumbers[randomNumbers.length - 1])
    }, 6000)
  }, [winBalls])

  const onPickWinBallAgain = useCallback(() => {
    console.log('onPickWinBallAgain')
    console.log(randomNumbers)
    setRandomNumbers(getRandomNumbers())
    setWinBalls([])
    setBonus(null)
    timeouts.current = []
  }, [randomNumbers])

  useEffect(() => {
    setRandomBalls()
    return () => {
      timeouts.current.forEach((i) => clearTimeout(i))
    }
  }, [timeouts.current])

  // componenetDidUpdate에서만 ajax원할때
  // useEffect(() => {
  //   if (!mount.current) {
  //     mount.current = true
  //   } else {
  //     ...ajax
  //   }
  // }, [바뀌는값])

  return (
    <>
      <h2>당첨숫자</h2>
      <Container>
        {winBalls.map((i) => (
          <Ball key={i} num={i}></Ball>
        ))}
      </Container>
      {bonus && (
        <>
          <h2>보너스볼</h2>
          <Ball num={bonus}></Ball>
          <PickAgain onClick={onPickWinBallAgain}>다시뽑기</PickAgain>
        </>
      )}
    </>
  )
}

export default Lotto
