import React from 'react'
import ReactDom from 'react-dom'
// import Lotto from './Components/Lotto'
import Lotto from './Components/FunctionalLotto'

ReactDom.render(<Lotto />, document.querySelector('#root'))
