import React, { useReducer, useCallback, useEffect, memo } from 'react'
import Table from './Table'

const style = 'border: 1px solid black'

export const SET_WINNER = 'SET_WINNER'
export const SELECTED_CELL = 'SELECTED_CELL'
export const CHANGE_TURN = 'CHANGE_TURN'
export const SET_DRAW = 'SET_DRAW'
export const RESET_GAME = 'RESET_GAME'

const initState = {
  turn: 'o',
  winner: '',
  draw: false,
  isFinished: false,
  tableData: [
    ['', '', ''],
    ['', '', ''],
    ['', '', '']
  ],
  recentCell: [-1, -1]
}

const reducer = (state, action) => {
  switch (action.type) {
    case SET_WINNER:
      return {
        ...state,
        winner: action.winner,
        isFinished: true
      }
    case SELECTED_CELL:
      const tableData = [...state.tableData]
      tableData[action.rowIndex] = [...tableData[action.rowIndex]]
      tableData[action.rowIndex][action.cellIndex] = state.turn
      return {
        ...state,
        tableData,
        recentCell: [action.rowIndex, action.cellIndex]
      }
    case CHANGE_TURN:
      return {
        ...state,
        turn: state.turn === 'o' ? 'x' : 'o'
      }
    case SET_DRAW:
      return {
        ...state,
        draw: action.draw,
        isFinished: true
      }
    case RESET_GAME:
      return {
        turn: action.turn,
        winner: '',
        draw: false,
        isFinished: false,
        tableData: [
          ['', '', ''],
          ['', '', ''],
          ['', '', '']
        ],
        recentCell: [-1, -1]
      }
    default:
      return state
  }
}

const Tictacto = () => {
  const [state, dispatch] = useReducer(reducer, initState)
  const { turn, winner, tableData, recentCell, draw, isFinished } = state

  const onResetClicked = () => {
    dispatch({ type: RESET_GAME, turn: turn === 'o' ? 'x' : 'o' })
  }

  useEffect(() => {
    const [row, cell] = recentCell
    if (row < 0) return
    let win = false
    if (
      tableData[row][0] === turn &&
      tableData[row][1] === turn &&
      tableData[row][2] === turn
    ) {
      win = true
    } else if (
      tableData[0][cell] === turn &&
      tableData[1][cell] === turn &&
      tableData[2][cell] === turn
    ) {
      win = true
    } else if (
      tableData[0][0] === turn &&
      tableData[1][1] === turn &&
      tableData[2][2] === turn
    ) {
      win = true
    } else if (
      tableData[0][2] === turn &&
      tableData[1][1] === turn &&
      tableData[2][0] === turn
    ) {
      win = true
    }
    if (win) {
      dispatch({
        type: SET_WINNER,
        winner: turn
      })
    } else {
      let allChecked = true
      tableData.forEach((r) =>
        r.forEach((c) => {
          if (!c) {
            allChecked = false
          }
        })
      )
      if (allChecked) {
        dispatch({ type: SET_DRAW, draw: true })
      } else {
        dispatch({ type: CHANGE_TURN })
      }
    }
  }, [tableData, isFinished])

  return (
    <>
      <h1>틱택토</h1>
      <Table
        dispatch={dispatch}
        tableData={tableData}
        isFinished={isFinished}
      ></Table>
      {winner && <div>{winner}가 이겼습니다.</div>}
      {draw && <div>비겼습니다.</div>}
      {isFinished && <button onClick={onResetClicked}>리셋</button>}
    </>
  )
}

export default memo(Tictacto)
