import React, { memo } from 'react'
import Tr from './Tr'

const style = {
  borderCollapse: 'collapse'
}

const Table = ({ tableData, dispatch, isFinished }) => {
  return (
    <table style={style}>
      {tableData.map((row, index) => (
        <Tr
          key={index}
          rowIndex={index}
          row={row}
          dispatch={dispatch}
          isFinished={isFinished}
        ></Tr>
      ))}
    </table>
  )
}

export default memo(Table)
