import React, { memo } from 'react'
import Td from './Td'

const Tr = ({ row, rowIndex, dispatch, isFinished }) => {
  return (
    <tr>
      {row.map((cell, index) => (
        <Td
          key={index + cell}
          rowIndex={rowIndex}
          cellIndex={index}
          cell={cell}
          dispatch={dispatch}
          isFinished={isFinished}
        ></Td>
      ))}
    </tr>
  )
}

export default memo(Tr)
