import React, { useCallback, memo } from 'react'
import { SELECTED_CELL } from './Tictacto'

const style = {
  color: 'red',
  border: '1px solid black',
  width: '100px',
  height: '100px',
  textAlign: 'center'
}

const Td = ({ rowIndex, cellIndex, cell, dispatch, isFinished }) => {
  const onCellClicked = useCallback(() => {
    if (cell || isFinished) return
    dispatch({ type: SELECTED_CELL, rowIndex, cellIndex })
  }, [cell, isFinished])
  return (
    <td onClick={onCellClicked} style={style}>
      {cell}
    </td>
  )
}

export default memo(Td)
