# React Playground

> React의 기본사용법과 설명은 [공식문서](https://reactjs.org/docs/getting-started.html)에 나옴. 추후 깊은 이해를 위해 대충 다독 정독 한번하자!

아래 웹게임들은 함수형 컴포넌트와 클래스 컴포넌트의 기본 사용법을 익히기 위한 간단한 게임들이다. 

리액트는 참고로 jsx를 사용하는데 jsx란 XML-like syntax extension to Javascript를 의미하는데, html을 js단에서 html과 비슷하게 사용할 수 있다. 

- 차이점은 종료태그가 반드시 있어야한다는 것 
- html의 기본속성중을 변경해서 사용한다는 것

ℹ️ JSX 사용하는 html 기본 속성

- class : className
- for : htmlFor

|이름|함수형컴포넌트|비고|
|---|---|---|
|구구단|TimeTable.jsx||
|함수형구구단|functionalTimesTable|useState, useRef, useEffect|
|끝말잇기|WordRelay.jsx||
|함수형끝말잇기|FunctionalWordRelay.jsx|useState, useRef, useEffect|
|숫자야구|number-baseball.jsx||
|함수형숫자야구|func-number-baseball.jsx|useState, useRef, useEffect, memo|
|반응속도체크|CheckReactionTime.jsx|pureComponent|
|함수형반응속도체크|FunctionalCheckReactionTime.jsx|useState, useRef, useEffect, memo|
|가위바위보|RSP.jsx|pureComponent|
|함수형가위바위보|FunctionalRSP.jsx|useState, useRef, useEffect, memo, debounce|
|로또|Lotto.jsx|pureComponent|
|함수형로또|FunctionalLotto.jsx|useState, useRef, useEffect, memo, useCallback, useMemo|
|함수형틱택토|Tictacto.jsx|useReducer, useEffect, memo, useCallback, useMemo|
|함수형지뢰찾기|SearchMine.jsx|useState, useReducer, useEffect, memo, createContext, useContext, useCallback, useMemo|

--- 

## Why REACT ?

> 리액트를 사용시 큰 이점 3가지는 아래와 같다.

- 사용자 경험: UI를 효율적으로 만들수 있고 사용자 경험이 좋도록 만들기위해서 사용한다. virtual dom
- 데이터 화면 일치: 데이터가 수정됬을시 화면에 바로 sync가 되도록 하는게 가장 중요하고 어려울수 있는데, 이를 리액트를 활용해서 해결
- 재사용 컴포넌트: 중복적인 코드 사용을 줄이기 위해서 (이건 번들러를 사용하는 모든 js소스들에게 해당하는데, 번들러가 중복된 모듈인경우 혹은 버전이 다른 중복 모듈의 경우 해결해줌.)

---

## state 변경

> 모던 js Framework의 경우 보통 상태변경시 직접 변경하지 않고 특정함수를 이용해서 상태를 변경하는데 리액트도 마찬가지이다.

### 클래스 컴포넌트의 경우

- setState를 할 때마다 render가 다시 실행된다. 따라서 성능을 생각한다면 이점을 유의하고 있어야함.
- 익명함수로 render부분에 구현된 부분이 있다면 render될때마다 생성이 된다. 때문에 함수들은 컴포넌트 상단에 **따로** 구현하는 것이 좋다.

### 함수형 컴포넌트의 경우

- state혹은 props이 변경될 때마다 함수형 컴포넌트 **자체 전부**가 재 실행된다. render뿐만아니라 전체가 다!!!
- 따로 구현한 함수들이 pure한 경우 따로 구현하는것 동일하지만 함수자체가 다시 재실행되므로 불필요한 호출이 반복될 수 있음도 명심할 필요가 있따.

따라서 이런 점을 보완하기 위해서 특정 hooks 혹들을 활용할 필요가 있다.

|명칭|목적|비고|
|---|---|---|
|useMemo|복잡한 함수의 결과값을 기억함|Context의 value값을 바인딩할 때 활용할 수 있다.단 재생성될 필요가 있는 여부를 잘 판단할 필요가 있다.|
|useCallback|함수자체를 기억함|함수형 컴포넌트 내부에서 사용하는 함수의 경우 useCallback을 활용하며, 재생성될 필요가 있는 여부를 잘 판단할 필요가 있다.|

#### 💁 useMemo
  > 복잡한 함수의 결과값을 기억함. 

  ```
  const cacheValue = useMemo(() => func(), [])
  useEffect 처럼 배열의 어떤 조건에 변경이 생기지 않으면 다시 실행되지 않음
  ```

#### 💁 useCallback
  > 함수자체를 기억함. 함수내부에 생성한 함수가 불필요하게 재생성될 수 있다. 이런 점을 방지하기 위해 사용함.
  ```
  const cacheValue = useCallback(() => func(), [])
  ```
  useEffect 처럼 배열의 어떤 조건에 변경이 생기지 않으면 다시 실행
  
  ***❗처음 해당 함수를 생성했을시 변수들의 정보를 계속 기억함. 이게 문제가 될시는 배열안에 해당 변수를 넣어줘야함.***

  ***❗자식에게 함수를 props로 전달해주는 경우에는 반드시 useCallback을 해줘야한다.***

---

## state 변경은 비동기로???

> 클래스형 컴포넌트이건 함수형 컴포넌트이건 state변경은 비동기로 진행이 된다. 따라서 동일함수내부의 setState를 여러번 반복한다할지라도 그것들은 동시에 병렬적으로 진행이 되므로 이전 상태값을 활용하여 **현재값을 변경할 때에는 함수형으로 상태를 변경해 줘야만한다!!!**

### 💁 클래스형 컴포넌트

```js
 this.setState((prev) => { return {value: prev.값 + 1}})
```

### 💁 함수형 컴포넌트

```js
  setValue((prev) => prev.value + 1)
```

### 💁 set이 비동기인 이유

> 상태를 변경할때마다 render가 일어난다. 비동기가 아니라면 잦은 state변경으로 성능이 치명적으로 안좋을 수 있지만 비동기로 진행이 된다면 상태변경이 동일 depth에서 여러번 일어나더라도 한번에 실행이 되므로 성능에 좋다.

---

## class 컴포넌트 라이프 사이클과 hooks

> componentWillUnmount를 제외한 Will 사이클들은 추후 deprecated될 예정.

constructor -> render -> ref -> componentDidMount
state or props가 변경시
-> shouldComponentUpdate -> render -> componenetDidUpdate
컴포넌트제거될시
-> componenetWillUnmount -> 소멸

### 💁 hooks는 class lifecycle과 완벽히 호환이 되지는 않는다.

> useEffect는 componentDidMount, componentDidUpdate, componentWillUnmount의 역할을 대행할 수 있다. 다만 몇가지 차이가 있다.

|class Component|Hooks|비고|
|---|---|---|
|componentDidMount|useEffect||
|componentDidUpdate|useEffect||
|componentDidWillUnmount|useEffect||

```
useEffect(() => {
  //componentDidMount, componentDidUpdate 의 로직을 이곳에 작성할 수 있다.
  return () => {}
  // return 구문은 componentWillUnmount의 대행부분이다. 따라서 이벤트 등록시에 이곳에 무조건 해제를 해준다.
},[])
// [] 부분이 빈배열이라면 componentDidMount의 역할만 수행
// [a, b] a, b가 변경시 componentDidUpdate가 수행됨. 
// 단 이때는 componentDidMount도 같이 수행됨을 명심 
```

- useEffect는 상태값들 혹은 변수들의 변경에 따라 실행이 됨.
- useEffect는 여러번 사용해도 됨.

### 💁 Hooks

- hooks는 절대 조건문안에 넣으면 안되.
- 다른 hooks 안에 hooks를 넣으면 안됨.
- 반복문에는 넣어도 되는데, 추천하진 않음.
- 무조건 최상위로 빼서 생성한다.
- 실행순서는 중요하기 때문에 꼭 유념한다. 

|hooks|용도|비고|
|---|---|---|
|useState|state를 변경||
|useEffect|componentDidMount, componentDidUpdate, componentWillUnmount|위에 참조|
|useRef|dom접근할 때 혹은 함수형프로그래밍 변수생성시 사용|일반 value값을 기억함|
|useMemo|함수값의 결과를 기억함|Context의 value바인딩시 사용|
|useCallback|함수자체를 기억함|함수형 컴포넌트내부의 함수생성시 사용|
|useReducer|여러 상태를 하나로 묶어담아 관리하며 dispatch를 통해서면 상태를 변경시킴|상태값이 많아지는 경우 사용하면 편함. **단, dispatch는 비동기로 실행된다.**|
|useContext|Context에 상태값을 담아두어 자식컴포넌트에 직접 전달하지 않아도 자식컴포넌트는 state를 사용할 있음||

--- 

## react-router

- react-router: 리액트 라우터의 기본적인 뼈대, 앱개발에서도 사용됨
- react-router-dom: 웹에서 사용하는 리액트 라우터 (react-router-native : 앱에서 사용하는 리액트 라우터)
웹에서 개발자가 실제로 사용하는 모듈은 react-router-dom이고 이것이 react-router를 필요로해서 종속시킨것이다.

### browser router
새로고침시 서버에서 난 몰라하면서 찾지 못한다. 하지만 방법은 있음. 옵션에 historyapifallback 을 하면 된다.... 

### hash router
새로고침시에도 잘 동작함. 
해쉬뒷부분은 프론트쪽 브라우저만 아는 부분임. 따라서 리액트라우터도 앎. 서버는 해쉬뒤에부분을 인식을 못함. 따라서 검색엔진에서 검색이 안됨. 왜냐면 웹서버는 해시라우터부분을 알지 못하기 때문에... 따라서 실무에서는 해쉬라우터를 사용하지 않음. 브라우저 라우터를 사용해도 SEO를 위해서는 따로 세팅이 필요하긴 함.

---

## 성능개선

> props, state가 변경되면 컴포넌트는 다시 렌더링이 된다. 따라서 쓸때없는 렌더링을 줄이려면 작은 컴포넌트 단위로 변경하여 **pureComponent** 혹은 **memo**를 이용해야한다. 클래스 컴포넌트의 경우 **shouldComponentUpdate** 함수를 이용해서 props state의 조건의 따라 렌더링을 다시 해줄 것인지 아닐것인지 판단하여 로직을 설정할수도 있다.

---

## Why Webpack?

단 node: js의 실행기, 런타임을 알아야한다.

- 동일한 참조모듈을 중복하지 않도록 중복제거하면서 하나의 js로 번들링해줌
- 쓸데없는 부분들을 제거: console.log 등

### 💁 webpack dev server

리액트 hot reload를 위해서 아래 모듈을 추가한다.
- webpack-dev-server

개발용 자동 리로드를 위한 개발 서버이다 이때 hot이라는 옵션을 쓸 수 있는데,
이 옵션을 true로 하면 state가 기억하고 있던 데이터는 그대로 유지가 된다.
false시에는 state가 기억되지 않고 새로 고침이 된다.

```
const path = require('path')
//const webpack = require('webpack')
//const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')

module.exports = {
  name: 'wordRelay config',
  mode: 'development',
  devtool: 'eval',
  resolve: {
    extensions: ['.jsx', '.js']
  },

  entry: {
    app: ['./client']
  },
  // test : 규칙을 적용할 파일들
  module: {
    rules: [
      {
        test: /\.jsx?/,
        loader: 'babel-loader',
        options: {
          presets: [
            [
              '@babel/preset-env',
              {
                targets: {
                  browsers: ['> 1% in KR']
                },
                debug: true
              }
            ],
            '@babel/preset-react'
          ],
          plugins: [
            '@babel/plugin-proposal-class-properties',
            //'react-refresh/babel'
          ]
        }
      }
    ]
  },
  plugins: [
    //new webpack.HotModuleReplacementPlugin(),
    //new ReactRefreshWebpackPlugin()
  ].filter(Boolean),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'app.js',
    publicPath: '/dist/'
  },
  devServer: {
    publicPath: '/dist/',
    hot: true
  }
}

```
---

## Babel

- babel-loader: 바벨과 웹팩을 활용해서 자바스크립트 파일을 변환해준다.
- @babel/core: 바벨의 핵심 코어
- preset: 플러그인들의 모임
  - @babel/preset-env: 브라우저 환경이(예전브라우저일지라도) 달라도 바벨이 es5로 변경해준다.
  - @babel/preset-react: 리액트 문법을 es5로 변경해준다.


## component lifecycle(class component 기준설명)

- Mount : 컴포넌트 인스턴스가 생성되서 DOM에 삽입될때 순서대로 호출됨

  - constructor()
  - static getDerivedStateFromProps()
  - **render()**
  - **componentDidMount()**

- Update : props또는 state가 변경되면 갱신이 발생함.

  - static getDerivedStateFromProps()
  - shouldComponentUpdate()
  - **render()**
  - getSnapshotBeforeUpdate()
  - **componentDidUpdate()**

- Mount 해제: 컴포넌트가 DOM상에서 제거될때 호출됨.

  - **componentWillUnmount()**

- 오류처리: 자식 컴포넌트를 렌더링하거나, 자식 컴포넌트가 생명주기 메서드를 호출하거나, 또는 자식 컴포넌트가 생성자 메서드를 호출하는 과정에서 오류가 발생했을 때 호출됨.

  - static getDerivedStateFromError()
  - **componentDidCatch()**

- 기타 API

  - **setState()**
  - forceUpdate()

- class 프로퍼티

  - defaultProps
  - displayName

- 인스턴스 프로퍼티
  - **props**
  - **state**