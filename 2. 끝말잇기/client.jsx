const ReactDom = require('react-dom')
const React = require('react')
// const WordRelay = require('./WordRelay')
const WordRelay = require('./FunctionalWordRelay')

ReactDom.render(<WordRelay />, document.querySelector('#root'))
