const React = require('react')
const { render } = require('react-dom/cjs/react-dom.development')

class WordRelay extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      word: '개발자',
      value: '',
      results: ''
    }
  }
  input
  onInputRef = (input) => {
    this.input = input
  }

  onSubmitHandler = (evt) => {
    evt.preventDefault()
    this.input.focus()
    if (
      this.state.value &&
      this.state.value[0] === this.state.word[this.state.word.length - 1]
    ) {
      this.setState({
        value: '',
        word: this.state.value,
        results: ''
      })
    } else {
      this.setState({
        value: '',
        results: '땡'
      })
    }
  }

  onChangeHandler = (evt) => {
    this.setState({
      value: evt.currentTarget.value
    })
  }

  componentDidMount() {
    this.input.focus()
  }

  render() {
    return (
      <>
        <h1>끝말잇기</h1>
        <div>단어: {this.state.word}</div>
        <form onSubmit={this.onSubmitHandler}>
          <input
            type="text"
            ref={this.onInputRef}
            value={this.state.value}
            onChange={this.onChangeHandler}
          />
        </form>
        <div>{this.state.results}</div>
      </>
    )
  }
}

module.exports = WordRelay
