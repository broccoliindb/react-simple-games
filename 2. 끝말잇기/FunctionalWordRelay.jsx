const React = require('react')
const { useEffect, useState, useRef } = React

const WordRelay = () => {
  const [value, setValue] = useState('')
  const [word, setWord] = useState('개발자')
  const [results, setResults] = useState('')

  const onInputRef = useRef(null)

  const onSumbitHandler = (evt) => {
    evt.preventDefault()
    if (value && value[0] === word[word.length - 1]) {
      setWord(() => value)
      setValue(() => '')
      setResults('')
    } else {
      setValue('')
      setResults('땡')
    }
  }

  const onChangeHandler = (evt) => {
    setValue(evt.currentTarget.value)
  }

  useEffect(() => {
    onInputRef.current.focus()
  }, [])

  return (
    <>
      <h1>(함수형) 끝말잇임</h1>
      <div>{word}</div>
      <form onSubmit={onSumbitHandler}>
        <input
          type="text"
          ref={onInputRef}
          value={value}
          onChange={onChangeHandler}
        />
      </form>
      <div>{results}</div>
    </>
  )
}

module.exports = WordRelay
