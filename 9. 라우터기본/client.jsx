import React from 'react'
import ReactDom from 'react-dom'
import Container from './components/Container'

ReactDom.render(<Container />, document.querySelector('#root'))
